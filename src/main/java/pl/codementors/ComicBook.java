package pl.codementors;

/**
 * Created by sit0 on 10.06.17.
 */
public class ComicBook extends Book {

    //yyyy-MM
    private String releaseDate;

    private String publishingSeries;

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPublishingSeries() {
        return publishingSeries;
    }

    public void setPublishingSeries(String publishingSeries) {
        this.publishingSeries = publishingSeries;
    }
    @Override
    public void print() {

        System.out.println("Typ: " + getClass().getSimpleName());
        System.out.println("Tytuł: " + getTitle());
        System.out.println("Autorzy:");
        for (Author author : getAuthors()){
            System.out.println("Imię i nazwisko autora " + author.getName() + author.getSurName());
            System.out.println("Pseudonim artystyczny: " + author.getStageName());
        }
        System.out.println("Data wydania: " + releaseDate);
        System.out.println("Wydawnictwo: " + publishingSeries);
        System.out.println("Okładka: " + getCover().toString());
        System.out.println("-----------------");

    }
}
