package pl.codementors;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by sit0 on 01.06.17.
 */
public class Book implements Serializable {

    public enum Cover {
        HARD,
        SOFT,
        INTEGRATED;
    }

    private String title;

    private Set<Author> authors;

    private int releaseYear;

    private Cover cover;

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

//    public Author getAuthor(int i){
//        return authors[i];
//    }

    public void setAuthors(HashSet<Author> authors) {
        this.authors = authors;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public Book(String title, HashSet<Author> authors, int releaseYear) {
        this.title = title;
        this.authors = authors;
        this.releaseYear = releaseYear;
    }

    public Book() {
        this.authors = new HashSet<>();
    }

    public void print() {

        System.out.println("Typ: " + getClass().getSimpleName());
        System.out.println("Tytuł: " + title);
        System.out.println("Autorzy:");
        for (Author author : authors){
            System.out.println("Imię i nazwisko autora " + author.getName() + author.getSurName());
            System.out.println("Pseudonim artystyczny: " + author.getStageName());
        }
        System.out.println("Data wydania: " + releaseYear);
        System.out.println("Okładka: " + cover.toString());
        System.out.println("-----------------");
    }
}
