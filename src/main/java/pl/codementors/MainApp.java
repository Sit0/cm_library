package pl.codementors;

import java.io.*;
import java.util.*;


/**
 * Created by sit0 on 01.06.17.
 */
public class MainApp {


    public static void main(String[] args) {

        Map<User,Library> userLibraryMap = readFromBinaryFile();

        Scanner inputScanner = new Scanner(System.in);

        System.out.println("Witam użytkowniku, podaj swój login");

        String userLogin = inputScanner.nextLine();
        Library library = checkLogin(userLogin, userLibraryMap);

        boolean runner = true;
        while (runner) {
            System.out.println("Witaj " + userLogin);
            System.out.println("MENU:\nWpisz 1 -> Dodaj pozycję do biblioteki\n" +
                    "Wpisz 2 -> Wypisz wybraną pozycję z biblioteki\n" +
                    "Wpisz 3 -> Wypisz całą bibliotekę\n" +
                    "Wpisz 4 -> Zmień użytkownika\n" +
                    "Wpisz 0 -> Wyjdz z programu\nWpisz numer operacji");

            switch (inputScanner.nextLine()) {

                case "1": {
                    Library.addBook(library);
                    break;
                }
                case "2": {
                    Library.printOneBook(library);
                    break;
                }
                case "3": {
                    Library.print(library);
                    break;
                }
                case "4": {
                    System.out.println("Podaj login nowego użytkownika" );
                    userLogin = inputScanner.nextLine();
                    library = checkLogin(userLogin, userLibraryMap);
                    break;
                }

                case "0": {
                    runner = false;
                    break;
                }
                default: {
                    System.out.println("Nie ma takiej pozycji w MENU. Wpisz raz jeszcze");
                }

            }
        }
        saveToBinaryFile(userLibraryMap);
        //library.saveToTxtFile();
    }

    public static Library checkLogin(String login, Map<User,Library> userLibraryMap){
        User checkUser = new User(login);
        if(userLibraryMap.containsKey(checkUser)){
            System.out.println("Login instnieje");
        }else{
            userLibraryMap.put(checkUser,new Library());
        }
        return userLibraryMap.get(checkUser);
    }

    public static Map<User,Library> readFromBinaryFile() {
        Map<User,Library> userLibraryMap;
        try (FileInputStream fis = new FileInputStream("libmem.bin");
             ObjectInputStream ois = new ObjectInputStream(fis);) {

            userLibraryMap = (Map<User, Library>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            userLibraryMap = new HashMap<>();
            System.out.println("Problem z odczytem binarnym");
            System.out.println(ex);
        }
        return userLibraryMap;
    }

    public static void saveToBinaryFile(Map<User,Library> map) {

        try (FileOutputStream fos = new FileOutputStream("libmem.bin");
             ObjectOutputStream ois = new ObjectOutputStream(fos);) {

            ois.writeObject(map);
        } catch (IOException ex) {
            System.out.println("Problem z zapisem binarnym");
            System.out.println(ex);
        }
    }
}